(function($, Drupal, drupalSettings) {
  'use strict';

  $(document).ready(function () {

    var drupalSettingsParams = drupalSettings['x_popup_window']['popup-window'];

    var $x_popup_overlay = $('.popup-overlay');
    var $x_popup_draggable = $('.popup-draggable');
    var $x_popup_header = $('.popup-draggable__header');
    var $x_popup_title = $('.popup-draggable__title');
    var $x_popup_content = $('.popup-draggable__content');

    var $elemToDrag = $x_popup_draggable;

    /*
     * +------------------------+
     * | Parameters application |
     * +------------------------+
     */
    $x_popup_draggable.css('width', drupalSettingsParams['window_width']);
    $x_popup_draggable.css('height', drupalSettingsParams['window_height']);
    $x_popup_content.css('background-color', drupalSettingsParams['content_bg']);
    $x_popup_header.css('background-color', drupalSettingsParams['title_area_bg']);
    $x_popup_title.css('color', drupalSettingsParams['title_area_fg']);
    $x_popup_title.text(drupalSettingsParams['title']);

    $('.pseudo-link').on('click', function () {
      $x_popup_overlay.show();
      $x_popup_draggable.show();
    });

    /*
     * +----------------+
     * | Overlay hiding |
     * +----------------+
     */
    function hideOverlay() {
      $x_popup_overlay.hide();
      $elemToDrag.hide();
    }

    $x_popup_overlay.on('click', hideOverlay);
    $('#x-popup-close').on('click', hideOverlay);

    /*
     * +----------------------+
     * |      Drag n drop     |
     * +----------------------+
     */
    var draggableContainer = document.getElementById('x-popup-draggable');
    var draggableHeader = document.getElementById('x-popup-header');

    var $innerWidth = $(window).width();
    var innerHeight = $(window).height();

    var $limitX = $innerWidth - $(draggableContainer).outerWidth();
    var $limitY = innerHeight - $(draggableContainer).outerHeight();

    Drag.init(draggableHeader, draggableContainer, 0, $limitX, 0, $limitY);

    $(draggableContainer).css('top', (innerHeight - $(draggableContainer).height()) / 2 + 'px');
    $(draggableContainer).css('left', (window.outerWidth - $(draggableContainer).width()) / 2 + 'px');

  });

})(jQuery, Drupal, drupalSettings);
