<?php

namespace Drupal\x_discount;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the X-Discount Entity entity.
 *
 * @see \Drupal\x_discount\Entity\XDiscountEntity.
 */
class XDiscountEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\x_discount\Entity\XDiscountEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished x-discount entity entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published x-discount entity entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit x-discount entity entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete x-discount entity entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add x-discount entity entities');
  }

}
