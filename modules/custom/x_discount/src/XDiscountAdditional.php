<?php

namespace Drupal\x_discount;

/**
 * Class XDiscountAdditional.
 *
 * Additional necessary procedures.
 *
 * @package Drupal\x_discount
 */
final class XDiscountAdditional {

  /**
   * Just takes a hello message from settings file.
   *
   * @return string
   *    Welcome message.
   */
  public static function xDiscountTakeDescriptionMessage() {
    return \Drupal::service('config.factory')
      ->getEditable('x_discount.settings')
      ->get('welcome_description');
  }

  /**
   * Take information about user that logged in.
   *
   * @return array
   *    Information about user. Contains:
   *    id       - user ID number;
   *    username - user name;
   *    code     - his discount code;
   *    exists   - condition if this user exists;
   *    account  - account from entity loaded by id;
   *    storage  - ref to an object of entity storage;
   */
  public static function xDiscountGetUserLoggedIn() {
    // TODO: To take the ID number right.
    $curr_user_obj = &\Drupal::currentUser();
    $uid = $curr_user_obj->id();
    $username = $curr_user_obj->getAccountName();

    $query = \Drupal::entityQuery('x_discount_entity');
    $query->condition('user_id', $uid);
    $entity_id = $query->execute();
    $entity_id = array_values($entity_id)[0];

    $storage = \Drupal::entityTypeManager()
      ->getStorage('x_discount_entity');
    $account = $storage->load($entity_id);
    $code = $account->field_discount_code->value;

    return [
      'id' => $uid,
      'username' => $username,
      'code' => $code,
      'exists' => !is_null($account),
      'account' => &$account,
      'storage' => &$storage,
    ];
  }

  /**
   * Just shows a message contains username and discount code.
   */
  public static function xDiscountShowMessage() {
    $token_service = \Drupal::token();
    $message = $token_service->replace(self::xDiscountTakeDescriptionMessage());
    drupal_set_message($message);
  }

  /**
   * Creates new record in entity.
   *
   * @param mixed $account
   *    Account object from hook_user_insert().
   */
  public static function xDiscountUserInsert($account) {
    $user = &self::xDiscountGetUserLoggedIn();
    $user['storage']->create([
      'user_id' => $account->id(),
      'field_discount_code' => (new XDiscountGenerator())->value(),
    ])->save();
  }

}
