<?php

namespace Drupal\x_discount;

/**
 * Class DiscountGenerator.
 *
 * Generates discount code.
 * Usage:
 * @code
 * $discount = (new DiscountGenerator(15))->value();
 * @endcode
 *
 * @package Drupal\x_discount
 */
class XDiscountGenerator {

  private $value;

  /**
   * That function tries to generate unique code.
   *
   * @param int $length
   *    Length of code.
   *
   * @return string
   *    Code that generated.
   */
  private static function generateValue($length): string {
    $half_of_len = ceil($length / 2);

    if (function_exists("openssl_random_pseudo_bytes")) {
      $bytes = openssl_random_pseudo_bytes($half_of_len);
    }

    elseif (function_exists("random_bytes")) {
      $bytes = random_bytes($half_of_len);
    }

    else {
      // Make a cryptographically unstable analog.
      $bytes = uniqid(rand());
    }

    return substr(bin2hex($bytes), 0, $length);
  }

  /**
   * DiscountGenerator constructor.
   *
   * @param int $length
   *    Length of generated code.
   */
  public function __construct($length = 10) {
    $this->value = self::generateValue($length);
    return $this;
  }

  /**
   * Get the result code.
   *
   * @return string
   *    Unique discount code value.
   */
  public function value(): string {
    return $this->value;
  }

}