<?php

namespace Drupal\x_discount\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class xdiscountentitySettingsForm.
 *
 * @ingroup x_discount
 */
class xdiscountentitySettingsForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'xdiscountentity_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::service('config.factory')
      ->getEditable('x_discount.settings')
      ->set('welcome_description',
        $form_state->getValue('xdiscountentity_welcome_description'))
      ->save();
    drupal_set_message(t('X-Discount configuration saved!'));
  }

  /**
   * Defines the settings form for X-Discount Entity entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['xdiscountentity_settings']['#markup'] = 'Settings form for X-Discount Entity entities. Manage field settings here.';

    $form['xdiscountentity_welcome_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Welcome message'),
      '#description' => $this->t('Welcome message text'),
      '#default_value' => \Drupal::service('config.factory')
        ->getEditable('x_discount.settings')
        ->get('welcome_description'),
    ];

    $form['xdiscountentity_x_discount_submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

}
