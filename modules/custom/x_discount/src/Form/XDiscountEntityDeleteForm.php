<?php

namespace Drupal\x_discount\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting X-Discount Entity entities.
 *
 * @ingroup x_discount
 */
class XDiscountEntityDeleteForm extends ContentEntityDeleteForm {


}
