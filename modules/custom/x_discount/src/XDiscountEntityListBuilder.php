<?php

namespace Drupal\x_discount;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\user\Entity\User;

/**
 * Defines a class to build a listing of X-Discount Entity entities.
 *
 * @ingroup x_discount
 */
class XDiscountEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('X-Discount Entity ID');
    $header['name'] = $this->t('User name');
    $header['code'] = $this->t('X-Discount code');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\x_discount\Entity\XDiscountEntity */
    $user_id = $entity->get('user_id')->entity->id();
    $account = User::load($user_id);

    $row['id'] = $entity->id();
    /*$row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.x_discount_entity.edit_form',
      ['x_discount_entity' => $entity->id()]
    );*/
    $row['name'] = $account->getAccountName();
    $row['code'] = $entity->field_discount_code->value;
    return $row + parent::buildRow($entity);
  }

}
