<?php

namespace Drupal\x_discount;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for x_discount_entity.
 */
class XDiscountEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
