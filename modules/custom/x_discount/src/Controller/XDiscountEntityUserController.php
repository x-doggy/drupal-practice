<?php

namespace Drupal\x_discount\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class XDiscountEntityUserController.
 *
 * @package Drupal\x_discount\Controller
 */
class XDiscountEntityUserController extends ControllerBase {

  /**
   * Returns able to render array describes user page.
   *
   * @return array
   *    Able to render array.
   */
  public function page() {
    $token_service = \Drupal::token();
    $message = $token_service->replace('
        <div class="x-discount-user">
          <p class="x-discount-user__content">
            Hello, [x_discount:username]!
          <p class="x-discount-user__content">
            Your discount code is: <pre><code>[x_discount:code]</code></pre>
        </div>
    ');
    return [
      '#markup' => $message,
    ];
  }

}
