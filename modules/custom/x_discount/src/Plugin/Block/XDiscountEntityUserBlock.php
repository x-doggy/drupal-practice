<?php

namespace Drupal\x_discount\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a X-Discount Block.
 *
 * @Block(
 *   id = "X-Discount user block",
 *   admin_label = @Translation("X-Discount user block"),
 * )
 */
class XDiscountEntityUserBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $token_service = \Drupal::token();
    $message = $token_service->replace('
        <div class="x-discount-user">
          <p class="x-discount-user__content">
            Hello, [x_discount:username]!
          <p class="x-discount-user__content">
            Your discount code is: [x_discount:code]
        </div>

      ');
    return [
      '#markup' => $message,
      '#cache' => ['max-age' => 0],
    ];
  }

}
