<?php

namespace Drupal\x_discount;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\x_discount\Entity\XDiscountEntityInterface;

/**
 * Defines the storage handler class for X-Discount Entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * X-Discount Entity entities.
 *
 * @ingroup x_discount
 */
interface XDiscountEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of X-Discount Entity revision IDs for a specific X-Discount Entity.
   *
   * @param \Drupal\x_discount\Entity\XDiscountEntityInterface $entity
   *   The X-Discount Entity entity.
   *
   * @return int[]
   *   X-Discount Entity revision IDs (in ascending order).
   */
  public function revisionIds(XDiscountEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as X-Discount Entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   X-Discount Entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\x_discount\Entity\XDiscountEntityInterface $entity
   *   The X-Discount Entity entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(XDiscountEntityInterface $entity);

  /**
   * Unsets the language for all X-Discount Entity with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
