<?php

namespace Drupal\x_discount;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\x_discount\Entity\XDiscountEntityInterface;

/**
 * Defines the storage handler class for X-Discount Entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * X-Discount Entity entities.
 *
 * @ingroup x_discount
 */
class XDiscountEntityStorage extends SqlContentEntityStorage implements XDiscountEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(XDiscountEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {x_discount_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {x_discount_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(XDiscountEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {x_discount_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('x_discount_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
