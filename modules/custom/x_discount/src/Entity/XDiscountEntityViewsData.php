<?php

namespace Drupal\x_discount\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for X-Discount Entity entities.
 */
class XDiscountEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
