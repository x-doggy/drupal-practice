<?php

namespace Drupal\x_discount\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the X-Discount Entity entity.
 *
 * @ingroup x_discount
 *
 * @ContentEntityType(
 *   id = "x_discount_entity",
 *   label = @Translation("X-Discount Entity"),
 *   handlers = {
 *     "storage" = "Drupal\x_discount\XDiscountEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\x_discount\XDiscountEntityListBuilder",
 *     "views_data" = "Drupal\x_discount\Entity\XDiscountEntityViewsData",
 *     "translation" = "Drupal\x_discount\XDiscountEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\x_discount\Form\XDiscountEntityForm",
 *       "add" = "Drupal\x_discount\Form\XDiscountEntityForm",
 *       "edit" = "Drupal\x_discount\Form\XDiscountEntityForm",
 *       "delete" = "Drupal\x_discount\Form\XDiscountEntityDeleteForm",
 *     },
 *     "access" = "Drupal\x_discount\XDiscountEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\x_discount\XDiscountEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "x_discount_entity",
 *   data_table = "x_discount_entity_field_data",
 *   revision_table = "x_discount_entity_revision",
 *   revision_data_table = "x_discount_entity_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer x-discount entity entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "revision" = "vid",
 *     "status" = "status",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *   },
 *   links = {
 *     "canonical" = "/xdiscount/x_discount_entity/{x_discount_entity}",
 *     "add-form" = "/xdiscount/x_discount_entity/add",
 *     "edit-form" = "/xdiscount/x_discount_entity/{x_discount_entity}/edit",
 *     "delete-form" = "/xdiscount/x_discount_entity/{x_discount_entity}/delete",
 *     "version-history" = "/xdiscount/x_discount_entity/{x_discount_entity}/revisions",
 *     "revision" = "/xdiscount/x_discount_entity/{x_discount_entity}/revisions/{x_discount_entity_revision}/view",
 *     "revision_revert" = "/xdiscount/x_discount_entity/{x_discount_entity}/revisions/{x_discount_entity_revision}/revert",
 *     "revision_delete" = "/xdiscount/x_discount_entity/{x_discount_entity}/revisions/{x_discount_entity_revision}/delete",
 *     "translation_revert" = "/xdiscount/x_discount_entity/{x_discount_entity}/revisions/{x_discount_entity_revision}/revert/{langcode}",
 *     "collection" = "/xdiscount/x_discount_entity",
 *   },
 *   field_ui_base_route = "x_discount_entity.settings"
 * )
 */
class XDiscountEntity extends RevisionableContentEntityBase implements XDiscountEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the x_discount_entity owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User ID'))
      ->setDescription(t('The user ID of author of the X-Discount entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the X-Discount Entity entity.'))
      ->setRevisionable(FALSE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the X-Discount Entity is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
