<?php

namespace Drupal\x_discount\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining X-Discount Entity entities.
 *
 * @ingroup x_discount
 */
interface XDiscountEntityInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the X-Discount Entity name.
   *
   * @return string
   *   Name of the X-Discount Entity.
   */
  public function getName();

  /**
   * Sets the X-Discount Entity name.
   *
   * @param string $name
   *   The X-Discount Entity name.
   *
   * @return \Drupal\x_discount\Entity\XDiscountEntityInterface
   *   The called X-Discount Entity entity.
   */
  public function setName($name);

  /**
   * Gets the X-Discount Entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the X-Discount Entity.
   */
  public function getCreatedTime();

  /**
   * Sets the X-Discount Entity creation timestamp.
   *
   * @param int $timestamp
   *   The X-Discount Entity creation timestamp.
   *
   * @return \Drupal\x_discount\Entity\XDiscountEntityInterface
   *   The called X-Discount Entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the X-Discount Entity published status indicator.
   *
   * Unpublished X-Discount Entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the X-Discount Entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a X-Discount Entity.
   *
   * @param bool $published
   *   TRUE to set this X-Discount Entity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\x_discount\Entity\XDiscountEntityInterface
   *   The called X-Discount Entity entity.
   */
  public function setPublished($published);

  /**
   * Gets the X-Discount Entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the X-Discount Entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\x_discount\Entity\XDiscountEntityInterface
   *   The called X-Discount Entity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the X-Discount Entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the X-Discount Entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\x_discount\Entity\XDiscountEntityInterface
   *   The called X-Discount Entity entity.
   */
  public function setRevisionUserId($uid);

}
