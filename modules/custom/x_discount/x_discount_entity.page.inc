<?php

/**
 * @file
 * Contains x_discount_entity.page.inc.
 *
 * Page callback for X-Discount Entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for X-Discount Entity templates.
 *
 * Default template: x_discount_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_x_discount_entity(array &$variables) {
  // Fetch XDiscountEntity Entity Object.
  $x_discount_entity = $variables['elements']['#x_discount_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
