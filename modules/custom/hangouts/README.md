CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers


INTRODUCTION
------------

 * This is a simple module that integrates Google Hangouts with your site.

 * Adds a new format "Hangouts button" for "string" field type (because Google ID is you e-mail address as a matter of fact).

 * Adds a new block containing Hangouts button whose size you can change.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


MAINTAINERS
-----------

 * This project is created by ADCI Solutions http://drupal.org/node/1542952 team.
