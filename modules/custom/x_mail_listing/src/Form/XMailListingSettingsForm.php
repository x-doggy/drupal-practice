<?php

namespace Drupal\x_mail_listing\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class XMailListingSettingsForm.
 *
 * @package Drupal\x_mail_listing
 */
class XMailListingSettingsForm extends ConfigFormBase {

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $langManager;

  /**
   * XMailListingSettingsForm constructor.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *    The mail manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *    The language manager.
   */
  public function __construct(MailManagerInterface $mailManager, LanguageManagerInterface $languageManager) {
    $this->mailManager = $mailManager;
    $this->langManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.mail'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['x_mail_listing.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'x_mail_listing_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $queue = \Drupal::queue('x_mail_listing_queue');
    if ($number_of_items = $queue->numberOfItems()) {
      $form['x_mail_listing_info_text'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<p>Current queue is launched, now remain @num elements.</p>', [
          '@num' => $number_of_items,
        ]),
      ];

      $form['x_mail_listing_cancel'] = [
        '#type' => 'submit',
        '#value' => $this->t('Cancel current queue.'),
        '#disable' => TRUE,
      ];
    }
    else {
      $form['x_mail_listing_settings_markup'] = [
        '#type' => 'markup',
        '#markup' => "<p>Submitting this form will process the queue.</p>
<p>You can use tokens, listed here:
<a href='https://www.drupal.org/node/390482#drupal7tokenslist' target='_blank'>available tokens</a>
(unfortunately, only for D7).</p>",
      ];

      $form['x_mail_listing_body'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Message body'),
        '#description' => $this->t('Message body with tokens.'),
        '#required' => TRUE,
        '#default_value' => \Drupal::config('x_mail_listing.settings')
          ->get('message_body'),
      ];

      $form['x_mail_listing_submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Send!'),
        '#disable' => TRUE,
      ];

    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::service('config.factory')
      ->getEditable('x_mail_listing.settings')
      ->set('message_body', $form_state->getValue('x_mail_listing_body'))
      ->save();

    $queue = \Drupal::queue('x_mail_listing_queue');
    if ('edit-x-mail-listing-cancel' === $form_state->getTriggeringElement()['#id']) {
      $queue->deleteQueue();
    }
    else {
      $list_of_users = \Drupal::database()
        ->select('users_field_data', 'u')
        ->fields('u', ['uid', 'name', 'mail'])
        ->condition('u.status', '1')
        ->execute()
        ->fetchAll();

      $queue->createQueue();

      foreach ($list_of_users as $row) {
        $queue->createItem([
          'uid' => $row->uid,
          'name' => $row->name,
          'email' => $row->mail,
        ]);
      }
    }

  }

}
