<?php

namespace Drupal\x_login_ajax;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AjaxContactSubmit.
 *
 * @package Drupal\x_login_ajax\XLoginAjaxSubmit
 */
class XLoginAjaxSubmit {

  /**
   * Ajax form submit callback.
   *
   * @param array $form
   *    Form we need.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *    Form state interface.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *    Response AJAX returns.
   */
  public function ajaxSubmitCallback(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();

    $username = $form_state->getValue('name');
    if ($form_state->get('uid')) {
      drupal_set_message(t("User with name @name exists!", ['@name' => $username]));
    }

    $message = [
      '#theme' => 'status_messages',
      '#message_list' => drupal_get_messages(),
      '#status_headings' => [
        'status' => t('Status message'),
        'error' => t('Error message'),
        'warning' => t('Warning message'),
      ],
    ];
    $messages = \Drupal::service('renderer')->render($message);
    $ajax_response->addCommand(new HtmlCommand('#' . Html::getClass($form['form_id']['#value']) . '-messages', $messages));
    return $ajax_response;
  }

}
