<?php

namespace Drupal\x_note;

use Drupal\node\Entity\Node;
use Drupal\Core\Config\Config;

/**
 * Class BatchProcedures.
 *
 * Contains batch-needed procedures wrapped into class.
 *
 * @package Drupal\x_note
 */
class BatchProcedures extends Config {

  /**
   * Sets 'N/A' status to all nodes.
   *
   * @param array $nids
   *    Array of nodes.
   * @param array $context
   *    Context.
   */
  public static function setNoneStatusOfNode($nids, &$context) {
    $message = 'Resetting status field state...';
    $results = [];
    foreach ($nids as $nid) {
      $node = Node::load($nid);
      $node->field_x_note_content_type_status = 'N/A';
      $results[] = $node->save();
    }
    $context['message'] = $message;
    $context['results'] = $results;
  }

  /**
   * Changes status to 'expired' if expiration time of the node was gone.
   *
   * @param array $nids
   *    Array of nodes.
   * @param array $context
   *    Context.
   */
  public static function changeStatusOfNode($nids, &$context) {
    $raw_date = \Drupal::service('config.factory')
      ->getEditable('x_note.settings')
      ->get('expiration_date');
    $config_date = $raw_date;
    $message = 'Updating state of \'status\' field...';
    $results = [];

    foreach ($nids as $nid) {
      $node = Node::load($nid);
      $node_date = $node->getCreatedTime();

      $node->field_x_note_content_type_status =
        ($config_date > $node_date) ? 'expired' : 'actual';

      $results[] = $node->save();
    }

    $context['message'] = $message;
    $context['results'] = $results;
  }

  /**
   * Callback of changing status.
   */
  public static function changeStatusToNodeFinishedCallback($success, $results, $operations) {
    if ($success) {
      $message = t('Success!');
      $type = 'status';
    }
    else {
      $message = t('Finished with an error!');
      $type = 'error';
    }
    drupal_set_message($message, $type);
  }

}
