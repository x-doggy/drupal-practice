<?php

namespace Drupal\x_note\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class x_noteSettingsForm.
 *
 * @ingroup x_note
 */
class XNoteSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['x_note.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'x_note_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['x_note_settings']['#markup'] =
      '<p>X-Note settings form. Manage field settings here.</p>';

    $form['x_note_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Expiration date'),
      '#description' => $this->t('Expiration date when status of all notes later that day becomes expired.'),
    ];

    $form['x_note_submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $stamp = strtotime($form_state->getValue('x_note_date'));
    $message = "X-Note configuration saved! Saved time stamp = $stamp";

    \Drupal::service('config.factory')
      ->getEditable('x_note.settings')
      ->set('expiration_date', $stamp)
      ->save();

    drupal_set_message($message);

    $nids = \Drupal::entityQuery('node')
      ->condition('type', 'x_note_content_type')
      ->sort('created', 'ASC')
      ->execute();

    $batch = [
      'title' => $this->t('Launching batch procedures...'),
      'operations' => [
        ['\Drupal\x_note\BatchProcedures::setNoneStatusOfNode', [$nids]],
        ['\Drupal\x_note\BatchProcedures::changeStatusOfNode', [$nids]],
      ],
    ];

    batch_set($batch);
  }

}
