var slides = document.getElementsByClassName("slideshow__myslides");
var dots = document.getElementsByClassName("slideshow__dot");

showSlides(1);

function currentSlide(n) {
    showSlides(n);
}

function showSlides(n) {
    var activeState = " slideshow__active";
    var slideIndex = n;
    var slidesLen = slides.length;

    if (n > slidesLen) slideIndex = 1;
    if (n < 1) slideIndex = slidesLen;
    for (var i = 0; i < slidesLen; i++) {
        slides[i].style.display = "none";
    }
    for (var i = 0, len = dots.length; i < len; i++) {
        dots[i].className = dots[i].className.replace(activeState, "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += activeState;
}
